//
//  GameCenterHelper.h
//  GameCenterTest
//
//  Created by Klemen Košir on 29/10/13.
//  Copyright (c) 2013 Klemen Košir. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>

@protocol GameCenterHelperDelegate <NSObject>

- (void)matchStarted;
- (void)matchEnded;
- (void)match:(GKMatch *)match didReceiveData:(NSData *)data fromPlayer:(NSString *)playerID;

@end

@interface GameCenterHelper : NSObject <GKMatchmakerViewControllerDelegate, GKMatchDelegate>{

    BOOL matchStarted;
    BOOL gameCenterAvailable;
    BOOL userAuthenticated;
    
}

@property (strong,nonatomic) UIViewController *presentingViewController;
@property (strong,nonatomic) GKMatch *match;
@property (weak,nonatomic) id <GameCenterHelperDelegate> delegate;

+ (GameCenterHelper *) sharedInstance;
- (void) authenticateLocalUser: (UIViewController *)mainView;
- (void) findMatchWithMinPlayers:(int)minPlayers maxPlayers:(int)maxPlayers viewController:(UIViewController *)viewController delegate:(id<GameCenterHelperDelegate>)theDelegate;


@end
