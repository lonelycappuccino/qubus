//
//  LCMenuScene.m
//  Qubus
//
//  Created by Klemen Košir on 17/02/14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import "LCMenuScene.h"
#import "LCGameOver.h"
#import "LCAdsHelper.h"


static const uint32_t playerCategory = 0x1 << 0;
static const uint32_t groundCategory = 0x1 << 1;
static const uint32_t cubeCategory = 0x1 << 2;
static const uint32_t gameOverCategory = 0x1 << 3;

@implementation LCMenuScene {
    SKSpriteNode *bg;
    CGFloat deviceFactor;
    CGFloat prevXPosition, prevWidth,randomHue, groundYPosition, lastHue;
    BOOL didTouchToJump, stillInTheAir, newHighscore,isGameOver,soundOn;
    int score;
    SKNode *world;
    SKSpriteNode *character;
    BOOL menuScene;
    UITapGestureRecognizer *tap;
    SKLabelNode *textLabel, *scoreLabel;
    LCGameOver *gameOverScene;
    SKEmitterNode *explosion;
    SKNode *lastTouched, *shareNode;
    BOOL menuJump;
    __strong SKAction *jumpSound, *explodeSound;
    SKTexture *screenshot;
}

-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        
        didTouchToJump = NO;
        stillInTheAir = NO;
        isGameOver = NO;
        menuScene = YES;
        newHighscore = NO;
        score = 0;
        
        
        deviceFactor = [self getDeviceFactor];
        [self loadData];
        [[LCAdsHelper sharedInstance]initBannerOnViewController];
        self.physicsWorld.gravity = CGVectorMake(0.0, -9.81*deviceFactor);
        self.physicsWorld.contactDelegate = self;
        
        randomHue = (arc4random() % 256)/256.0;
        bg = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithHue:randomHue saturation:0.15 brightness:1.0 alpha:1.0] size:self.size];
        [bg setPosition:CGPointMake(self.size.width/2, self.size.height/2)];
        [self addChild:bg];
        
        world = [SKNode node];
        [self addChild:world];
        
        character = [SKSpriteNode spriteNodeWithImageNamed:@"character"];
        [character setPhysicsBody:[SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake((deviceFactor == 1.0 ? 45.0 : 98.0), (deviceFactor == 1.0 ? 45.0 : 98.0))]];
        [character.physicsBody setCategoryBitMask:playerCategory];
        [character.physicsBody setContactTestBitMask:groundCategory | gameOverCategory | cubeCategory];
        [character.physicsBody setFriction:1.0];
        [character.physicsBody setRestitution:0.0];
        [character.physicsBody setMass:10.0];
        [world addChild:character];
    
        SKTexture *frame1 = [SKTexture textureWithImageNamed:@"character"];
        SKTexture *frame2 = [SKTexture textureWithImageNamed:@"character_blink"];
        NSArray *characterAnimationFrames = @[frame1,frame2,frame1];
        SKAction *animation = [SKAction repeatActionForever:[SKAction sequence:@[[SKAction animateWithTextures:characterAnimationFrames timePerFrame:0.25],
                                                                                 [SKAction waitForDuration:4.0]]]];
        [character runAction:animation];
        
        double length = (115 + arc4random() % 205)*deviceFactor;
        SKSpriteNode *ground = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithWhite:0.001 alpha:1.0] size:CGSizeMake(length, 233.0*deviceFactor)];
        [ground setColor:[UIColor colorWithHue:randomHue saturation:1.0 brightness:0.85 alpha:1.0]];
        [ground setPosition:CGPointMake(ground.size.width/2,ground.size.height/3)];
        [ground setPhysicsBody:[SKPhysicsBody bodyWithRectangleOfSize:ground.size]];
        [ground.physicsBody setCategoryBitMask:groundCategory];
        [ground.physicsBody setDynamic:NO];
        [ground.physicsBody setRestitution:0.0];
        [ground.physicsBody setFriction:1.0];
        [ground setName:@"ground"];
        [world addChild:ground];
        
        prevWidth = ground.size.width;
        prevXPosition = ground.position.x;
        groundYPosition = ground.position.y;
        
        [self createGround];
        [self createGround];
        
        
        [character setPosition:CGPointMake(character.size.width/2, ground.size.height/6*5+character.size.height)];
        
        [character runAction:[SKAction repeatActionForever:[SKAction sequence:@[[SKAction waitForDuration:3.0],
                                                                                [SKAction runBlock:^{
            int mod = 1000 + arc4random() % 3500;
            [character.physicsBody applyImpulse:CGVectorMake(mod*deviceFactor, mod*0.9*deviceFactor)];
        }]]]] withKey:@"jumping"];
        menuJump = YES;
        
        SKLabelNode *infoLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura-Medium"];
        [infoLabel setFontColor:[UIColor darkGrayColor]];
        [infoLabel setFontSize:15.0*deviceFactor];
        [infoLabel setText:@"Lonely Cappuccino"];
        [infoLabel setPosition:CGPointMake(self.size.width/2, 15*deviceFactor)];
        [self addChild:infoLabel];
        
        SKSpriteNode *gameCenterIcon = [SKSpriteNode spriteNodeWithImageNamed:@"gameCenter"];
        [gameCenterIcon setName:@"gameCenter"];
        [gameCenterIcon setAnchorPoint:CGPointMake(0.0, 0.0)];
        [gameCenterIcon setPosition:CGPointMake(4.0*deviceFactor, 4.0*deviceFactor)];
        [self addChild:gameCenterIcon];
        
        SKSpriteNode *title = [SKSpriteNode spriteNodeWithImageNamed:@"title"];
        [title setName:@"title"];
        [title setPosition:CGPointMake(self.size.width/2+(deviceFactor == 1.0 ? 1.0 : 0.0), self.size.height/2+(deviceFactor == 1.0 ? 80.0 : 203.5))];
        [self addChild:title];
        [title setScale:deviceFactor];
        [title.texture setFilteringMode:SKTextureFilteringNearest];
        /*
        [title runAction:[SKAction repeatActionForever:[SKAction sequence:@[[SKAction scaleTo:0.96 duration:0.9],
                                                                            [SKAction scaleTo:1.0 duration:0.8]]]]];
        */
        SKSpriteNode *soundOnOffButton = [SKSpriteNode spriteNodeWithImageNamed:(soundOn ? @"soundOn" : @"soundOff")];
        [soundOnOffButton setName:@"soundButton"];
        [soundOnOffButton setPosition:CGPointMake(self.size.width-4.0*deviceFactor, 4.0*deviceFactor)];
        [soundOnOffButton setAnchorPoint:CGPointMake(1.0, 0.0)];
        [soundOnOffButton.texture setFilteringMode:SKTextureFilteringNearest];
        [soundOnOffButton setScale:deviceFactor*0.8];
        [self addChild:soundOnOffButton];
        
        textLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura-Medium"];
        [textLabel setFontColor:[UIColor darkGrayColor]];
        [textLabel setFontSize:30.0*deviceFactor];
        [textLabel setText:@"Tap to jump"];
        [textLabel setPosition:CGPointMake(self.size.width/2, self.size.height/2)];
        [self addChild:textLabel];
        
        [textLabel runAction:[SKAction repeatActionForever:[SKAction sequence:@[[SKAction scaleTo:0.96 duration:0.9],
                                                                            [SKAction scaleTo:1.0 duration:0.8]]]]];
        
        AVPlayer *player = [[AVPlayer alloc]init];
        [player setVolume:0.5];
        
        NSString *path = [[NSBundle mainBundle]pathForResource:@"Explosion" ofType:@"sks"];
        explosion = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
        [explosion setParticleBirthRate:[explosion particleBirthRate]*deviceFactor];
        [explosion setParticleScale:[explosion particleScale]*deviceFactor];
        [explosion resetSimulation];
    }
    return self;
}

-(void)createGround {
    int min = 175 - (5*score);
    int max = 60 + 10*score;
    if (min <= 90) {
        min = 90;
    }
    if (max > 100) {
        max = 100;
    }
    randomHue = (arc4random() % 256)/256.0;
    double length = (min + arc4random() % min)*deviceFactor;
    SKSpriteNode *ground = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithWhite:0.001 alpha:1.0] size:CGSizeMake(length, 233.0*deviceFactor)];
    [ground setColor:[UIColor colorWithHue:randomHue saturation:1.0 brightness:0.85 alpha:1.0]];
    [ground setColorBlendFactor:1.0];
    [ground setPosition:CGPointMake(ground.size.width/2+prevXPosition+prevWidth/2 + (arc4random() % max + max)*deviceFactor,ground.size.height/3)];
    [ground setPhysicsBody:[SKPhysicsBody bodyWithRectangleOfSize:ground.size]];
    [ground.physicsBody setCategoryBitMask:groundCategory];
    [ground.physicsBody setDynamic:NO];
    [ground.physicsBody setRestitution:0.0];
    [ground.physicsBody setFriction:1.0];
    [ground setName:@"ground"];
    [world addChild:ground];
    
    prevWidth = ground.size.width;
    prevXPosition = ground.position.x;
    groundYPosition = ground.position.y;
    
    if (menuScene) {
        return;
    }
    
    int minHeight = 13 - (score * 3);
    if (minHeight < 90) {
        minHeight = 90;
    }
    
    
    if (!(arc4random() % 2) && score > 0) {
        SKSpriteNode *top = [SKSpriteNode spriteNodeWithColor:[UIColor whiteColor] size:CGSizeMake(90.0*deviceFactor, 90.0*deviceFactor)];
        [top setColor:[UIColor colorWithHue:randomHue saturation:1.0 brightness:0.85 alpha:1.0]];
        [top setColorBlendFactor:1.0];
        [top setAnchorPoint:CGPointMake(0.5,0.5)];
        [top setPhysicsBody:[SKPhysicsBody bodyWithRectangleOfSize:top.size]];
        [top.physicsBody setDynamic:NO];
        [top.physicsBody setRestitution:0.0];
        [top.physicsBody setCategoryBitMask:cubeCategory];
        [top setPosition:CGPointMake(prevXPosition-prevWidth/2+top.size.width/2, groundYPosition+ground.size.height/2+minHeight*deviceFactor+top.size.height/2)];
        
        [world addChild:top];
    }
}

-(CGFloat)getDeviceFactor {
    return ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone ? 1.0 : 2.0);
}

-(void)didBeginContact:(SKPhysicsContact *)contact {
    if (!lastTouched) {
        lastTouched = (contact.bodyA.categoryBitMask == groundCategory ? contact.bodyA.node : contact.bodyB.node);
    }
    
    if (contact.bodyA.categoryBitMask == cubeCategory ||contact.bodyB.categoryBitMask == cubeCategory) {
        didTouchToJump = NO;
        stillInTheAir = NO;
    }
    
    if (contact.bodyA.categoryBitMask == groundCategory ||contact.bodyB.categoryBitMask == groundCategory) {
        didTouchToJump = NO;
        menuJump = NO;
        stillInTheAir = NO;
        CGPoint cameraPositionInScene = [character.scene convertPoint:character.position fromNode:character.parent];
        [world runAction:[SKAction moveTo:CGPointMake(world.position.x - cameraPositionInScene.x+self.size.width/5, world.position.y) duration:0.5]];
        if (![character actionForKey:@"jump"]) {
            if (character.zRotation > -1.0 && character.zRotation < 1.0) {
                [character runAction:[SKAction sequence:@[[SKAction scaleYTo:0.9 duration:0.05],
                                                          [SKAction scaleYTo:1.0 duration:0.07]]] withKey:@"jump"];
            }
            CGFloat hue = 0.0;
            if ([contact.bodyA.node.name isEqualToString:@"ground"]) {
                [[((SKSpriteNode*)contact.bodyA.node) color] getHue:&hue saturation:nil brightness:nil alpha:nil];
            }
            else if ([contact.bodyB.node.name isEqualToString:@"ground"]) {
                [[((SKSpriteNode*)contact.bodyB.node) color] getHue:&hue saturation:nil brightness:nil alpha:nil];
            }
            //if (character.position.y < 216.0+(220.0*(deviceFactor-1.0))) {
                [bg runAction:[SKAction colorizeWithColor:[UIColor colorWithHue:hue saturation:0.15 brightness:1.0 alpha:1.0] colorBlendFactor:1.0 duration:0.5]];
                lastHue = hue;
            //}
        }
        
        if (lastTouched && (contact.bodyA.categoryBitMask == groundCategory ? contact.bodyA.node != lastTouched : contact.bodyB.node != lastTouched) && character.position.y > 216.0+(220.0*(deviceFactor-1.0)) && character.position.y < 230.0+(220.0*(deviceFactor-1.0))) {
            score++;
            lastTouched = (contact.bodyA.categoryBitMask == groundCategory ? contact.bodyA.node : contact.bodyB.node);
            [scoreLabel setText:[NSString stringWithFormat:@"%d",score]];
            if (score > [[_data objectForKey:@"highscore"]intValue]) {
                [scoreLabel setFontColor:[UIColor greenColor]];
            }
        }
    }
    else if ((contact.bodyA.categoryBitMask == gameOverCategory || contact.bodyB.categoryBitMask == gameOverCategory) && !menuJump) {
        if (!explosion.parent) {
            screenshot = [self.view textureFromNode:self];
            isGameOver = YES;
            [explosion setPosition:CGPointMake(character.position.x, character.position.y-20.0)];
            [world addChild:explosion];
            if (soundOn) {
                [self runAction:explodeSound];
                soundOn = NO;
            }
            [character runAction:[SKAction fadeAlphaTo:0.0 duration:0.03]];
            [self performSelector:@selector(gameOver) withObject:nil afterDelay:1.0];
        }
    }
}

-(void)didEndContact:(SKPhysicsContact *)contact {
    stillInTheAir = YES;
    
    
    if (![character actionForKey:@"jump"]) {
        [character runAction:[SKAction sequence:@[[SKAction scaleYTo:0.80 duration:0.1],
                                                  [SKAction scaleYTo:1.0 duration:0.03]]] withKey:@"jump"];
    }
}

-(void)didSimulatePhysics {
    CGPoint cameraPositionInScene = [character.scene convertPoint:character.position fromNode:character.parent];

    if (world.position.x - cameraPositionInScene.x+self.size.width*4/5 < world.position.x) {
        world.position = CGPointMake(world.position.x - cameraPositionInScene.x+self.size.width*4.0/5.0, world.position.y);
    }
}

-(void)removeAllGestures {
    for (UIGestureRecognizer *gesture in [self.view gestureRecognizers]) {
        [self.view removeGestureRecognizer:gesture];
    }
}

-(void)enableUserInteraction {
    [self.view setUserInteractionEnabled:YES];
}

-(void)didMoveToView:(SKView *)view {
    [self removeAllGestures];
    [self performSelector:@selector(enableUserInteraction) withObject:nil afterDelay:1];
    if (soundOn) {
        jumpSound = [SKAction playSoundFileNamed:@"Jump.mp3" waitForCompletion:NO];
        explodeSound = [SKAction playSoundFileNamed:@"Explosion.mp3" waitForCompletion:NO];
    }
    tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapHandler:)];
    [self.view addGestureRecognizer:tap];
    gameOverScene = [LCGameOver sceneWithSize:self.size];
}

-(void)tapHandler:(UITapGestureRecognizer*)gesture {
    CGPoint location = [self convertPointFromView:[gesture locationInView:self.view]];
    if ([[self childNodeWithName:@"gameCenter"]containsPoint:location]) {
        [self showGameCenter];
    }
    else if ([[self childNodeWithName:@"soundButton"]containsPoint:location]) {
        if (soundOn) {
            [[self childNodeWithName:@"soundButton"] runAction:[SKAction setTexture:[SKTexture textureWithImageNamed:@"soundOff"]]];
            soundOn = NO;
        }
        else {
            [[self childNodeWithName:@"soundButton"] runAction:[SKAction setTexture:[SKTexture textureWithImageNamed:@"soundOn"]]];
            soundOn = YES;
        }
        [_data setObject:[NSNumber numberWithBool:soundOn] forKey:@"soundOn"];
        [self writeUserData];
    }
}

-(void)restart {
    [self setPhysicsBody:[SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(self.size.width*2.0, 1.0)]];
    [self.physicsBody setCategoryBitMask:gameOverCategory];
    [self.physicsBody setDynamic:NO];
    [character removeActionForKey:@"jumping"];
    menuScene = NO;
    
    scoreLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura-Medium"];
    [scoreLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
    [scoreLabel setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeCenter];
    scoreLabel.fontColor = [UIColor darkGrayColor];
    scoreLabel.text = @"0";
    scoreLabel.fontSize = 40.0*deviceFactor;
    scoreLabel.position = CGPointMake(CGRectGetMidX(self.frame),
                                      self.size.height-100.0*deviceFactor);
    [self addChild:scoreLabel];

    [[self childNodeWithName:@"title"] removeFromParent];
    [[self childNodeWithName:@"shareNode"] removeFromParent];
    [[self childNodeWithName:@"soundButton"] removeFromParent];

    [textLabel removeFromParent];
    
    [self removeAllGestures];
}

-(void)startGame {
    score = 0;
    [self setPhysicsBody:[SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(self.size.width*2.0, 1.0)]];
    [self.physicsBody setCategoryBitMask:gameOverCategory];
    [self.physicsBody setDynamic:NO];
    [character removeActionForKey:@"jumping"];
    menuScene = NO;
    
    scoreLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura-Medium"];
    [scoreLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
    [scoreLabel setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeCenter];
    scoreLabel.fontColor = [UIColor darkGrayColor];
    scoreLabel.text = @"0";
    scoreLabel.fontSize = 45.0*deviceFactor;
    scoreLabel.position = CGPointMake(CGRectGetMidX(self.frame),
                                      self.size.height-100.0*deviceFactor);
    scoreLabel.alpha = 0.0;
    [self addChild:scoreLabel];
    [scoreLabel runAction:[SKAction fadeAlphaTo:1.0 duration:0.2] completion:^{
    }];
    [[self childNodeWithName:@"title"]runAction:[SKAction fadeAlphaTo:0.0 duration:0.3] completion:^{
        [[self childNodeWithName:@"title"] removeFromParent];
    }];
    [[self childNodeWithName:@"shareNode"]runAction:[SKAction fadeAlphaTo:0.0 duration:0.25] completion:^{
        [[self childNodeWithName:@"shareNode"] removeFromParent];
    }];
    [[self childNodeWithName:@"soundButton"]runAction:[SKAction fadeAlphaTo:0.0 duration:0.25] completion:^{
        [[self childNodeWithName:@"soundButton"] removeFromParent];
    }];
    [[self childNodeWithName:@"gameCenter"]runAction:[SKAction fadeAlphaTo:0.0 duration:0.25] completion:^{
        [[self childNodeWithName:@"gameCenter"] removeFromParent];
    }];
    [textLabel runAction:[SKAction fadeAlphaTo:0.0 duration:0.2] completion:^{
        [textLabel removeFromParent];
    }];
    
    [self removeAllGestures];
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint location = [[touches anyObject]locationInNode:self];
    if ((CGRectContainsPoint(CGRectMake(0.0, 50.0*deviceFactor, self.size.width, self.size.height-100.0*deviceFactor), location)) || !menuScene) {
        if (menuScene) {
            [self startGame];
        }
        if (stillInTheAir || isGameOver || location.y > self.size.height-20.0 || location.y < 20.0) {
            return;
        }
        if (soundOn) {
            [self runAction:jumpSound];
        }
        didTouchToJump = YES;
        [world removeAllActions];
        [character runAction:[SKAction sequence:@[[SKAction scaleYTo:0.80 duration:0.1],
                                                  [SKAction scaleYTo:1.0 duration:0.03]]]];
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    didTouchToJump = NO;
}

-(void)setData:(NSMutableDictionary *)dataTemp {
    soundOn = [[dataTemp objectForKey:@"soundOn"]boolValue];
    _data = dataTemp;
}

-(void)gameOver {
    if ([[_data objectForKey:@"highscore"]integerValue] < score) {
        [self reportData];
    }
    else {
        [_data setObject:[NSNumber numberWithBool:YES] forKey:@"reported"];
    }
    [self writeUserData];
    [self.view setUserInteractionEnabled:YES];
    [gameOverScene setScore:score];
    [gameOverScene setData:_data];
    [gameOverScene setNewHighscore:newHighscore];
    [gameOverScene setColor:[UIColor colorWithHue:lastHue saturation:0.2 brightness:0.8 alpha:1.0]];
    [self.view setUserInteractionEnabled:NO];
    [gameOverScene setScreenshot:screenshot];
    [self.view presentScene:gameOverScene];
    
}


-(void)loadData {
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDir = [paths objectAtIndex:0];
    NSString *path = [documentDir stringByAppendingPathComponent:@"data.plist"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath:path] == NO) {
        NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"data" ofType:@"plist"];
        [fileManager copyItemAtPath:bundlePath toPath:path error:&error];
        NSLog(@"%@",error);
    }
    _data = [NSMutableDictionary dictionaryWithContentsOfFile:path];
    soundOn = [[_data objectForKey:@"soundOn"]boolValue];
    if ([[_data objectForKey:@"reported"]boolValue] == NO) {
        [self reportData];
    }
}

-(void)reportData {
    newHighscore = YES;
    GKScore *gkScore = [[GKScore alloc]initWithLeaderboardIdentifier:@"leaderboard_1"];
    [gkScore setValue:[[_data objectForKey:@"highscore"]integerValue]];
    [_data setObject:[NSNumber numberWithInt:score] forKey:@"highscore"];
    [GKScore reportScores:@[gkScore] withCompletionHandler:^(NSError *error) {
        if (error) {
            NSLog(@"%@",error);
            [_data setObject:[NSNumber numberWithBool:NO] forKey:@"reported"];
        }
        else {
            NSLog(@"Score reported successfully");
            [_data setObject:[NSNumber numberWithBool:YES] forKey:@"reported"];
        }
    }];
}

-(void)showGameCenter {
    GKGameCenterViewController *gameCenterViewController = [[GKGameCenterViewController alloc]init];
    if (gameCenterViewController != nil) {
        gameCenterViewController.gameCenterDelegate = self;
        [gameCenterViewController setViewState:GKGameCenterViewControllerStateDefault];
        UIViewController *viewController = self.view.window.rootViewController;
        //NSLog(@"%@",viewController);
        [viewController presentViewController:gameCenterViewController animated:YES completion:nil];
        //NSLog(@"Should show GC");
    }
}

-(void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController {
    [gameCenterViewController dismissViewControllerAnimated:YES completion:nil];
}

-(void)update:(NSTimeInterval)currentTime {
    /*[world enumerateChildNodesWithName:@"ground" usingBlock:^(SKNode *node, BOOL *stop) {
        if ([self convertPoint:node.position fromNode:world].x < -140.0*deviceFactor) {
            [node removeFromParent];
            [self createGround];
        }
    }];*/
    for (SKNode *node in world.children) {
        if ([node.name isEqualToString:@"ground"]) {
            if ([self convertPoint:node.position fromNode:world].x < -140.0*deviceFactor) {
                [node removeFromParent];
                [self createGround];
            }
        }
    }
    if (character.position.y < 0.0 && menuScene) {
        __block SKNode *closestGround;
        [world enumerateChildNodesWithName:@"ground" usingBlock:^(SKNode *node, BOOL *stop) {
            if (!closestGround && node.position.x > character.position.x) {
                closestGround = node;
            }
            else if (node.position.x-node.frame.size.width/2 > character.position.x && closestGround.position.x > node.position.x) {
                closestGround = node;
            }
        }];
        [character setPosition:CGPointMake(closestGround.position.x-closestGround.frame.size.width/3, self.size.height/2)];
        character.physicsBody.velocity = CGVectorMake(0.0, 0.0);
        character.physicsBody.angularVelocity = 0.0;
        [character setZRotation:0.0];
        return;
    }
    if (didTouchToJump && character.position.y > 216.0+(220.0*(deviceFactor-1.0))) {
        if (character.physicsBody.velocity.dx < 500.0*deviceFactor) {
            [character.physicsBody applyForce:CGVectorMake(33000.0*deviceFactor, 42000.0*deviceFactor)];
        }
    }
    else if (character.position.y < 216.5+(220.0*(deviceFactor-1.0))) {
        stillInTheAir = NO;
    }

    //If the character is not facing the wrong way when standing still rotate him
    if (((int)(character.physicsBody.velocity.dx*100.0))/100.0 == 0.0 && ((int)(character.physicsBody.velocity.dy)*100.0)/100.0 == 0.0 && ((int)(character.zRotation*10.0))/10.0 != 0.0 && ![character actionForKey:@"rotate"] && !stillInTheAir) {
        self.physicsWorld.gravity = CGVectorMake(0.0, 0.0);
        character.physicsBody.resting = YES;
        stillInTheAir = YES;
        [character runAction:[SKAction sequence:@[[SKAction moveToY:character.position.y+15.0 duration:0.4],
                                                  [SKAction rotateToAngle:0.0 duration:0.4],
                                                  [SKAction runBlock:^{
            [character.physicsBody setResting:NO];
            [self.physicsWorld setGravity:CGVectorMake(0.0, -9.81*deviceFactor)];
            [self.view setUserInteractionEnabled:YES];
        }]]] withKey:@"rotate"];
        //[self.view setUserInteractionEnabled:NO];
    }
    
}

-(void)writeUserData {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDir = [paths objectAtIndex:0];
    NSString *userDataPath = [documentDir stringByAppendingPathComponent:@"data.plist"];
    if([_data writeToFile:userDataPath atomically:YES]) {
        NSLog(@"Writing data successfull");
    }
    else {
        NSLog(@"Writing data failed");
    }
}

@end
