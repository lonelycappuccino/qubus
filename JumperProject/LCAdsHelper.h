//
//  LCAdsHelper.h
//
//  Created by Klemen Košir on 07/02/14.
//  Copyright (c) 2014 Lonely Cappucino. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <iAd/iAd.h>
#import "GADBannerView.h"
#import "GADInterstitial.h"

@interface LCAdsHelper : NSObject <ADBannerViewDelegate,ADInterstitialAdDelegate,GADBannerViewDelegate,GADInterstitialDelegate>

@property (strong, nonatomic, setter = passViewController:) UIViewController *viewController;
@property (nonatomic,assign) BOOL iAdAvailable;
@property (nonatomic,assign) BOOL bannerAdLoaded;
@property (nonatomic,assign) BOOL interstitialAdLoaded;
@property (nonatomic,assign) BOOL didShowInterstitialAd;
@property (nonatomic,assign) int adPositionInt;

+(LCAdsHelper*)sharedInstance;
-(void)initBannerOnViewController;
-(void)hideBanner;
-(void)showBannerWithPosition:(int)positionInt;
-(void)initInterstitialAdOnViewController;
-(void)presentInterstitialAd;
-(void)initAdMobBanner;

@end
