//
//  LCViewController.m
//  Qubus
//
//  Created by Klemen Košir on 14/02/14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import "LCViewController.h"
#import "LCMenuScene.h"
#import "LCAdsHelper.h"
#import "GameCenterHelper.h"

@implementation LCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[GameCenterHelper sharedInstance]authenticateLocalUser:self];
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
}

-(void)viewDidLayoutSubviews {
    // Configure the view.
    [super viewDidLayoutSubviews];
    [[LCAdsHelper sharedInstance]passViewController:self];
    SKView * skView = (SKView *)self.view;
    if (!skView.scene) {
        //skView.showsFPS = YES;
        //skView.showsNodeCount = YES;
        
        // Create and configure the scene.
        LCMenuScene * scene = [LCMenuScene sceneWithSize:skView.bounds.size];
        scene.scaleMode = SKSceneScaleModeAspectFill;
        
        // Present the scene.
        //[scene showSplashScreen];
        [skView presentScene:scene];
    }
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

@end
