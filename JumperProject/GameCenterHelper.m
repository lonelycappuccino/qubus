//
//  GameCenterHelper.m
//  GameCenterTest
//
//  Created by Klemen Košir on 29/10/13.
//  Copyright (c) 2013 Klemen Košir. All rights reserved.
//

#import "GameCenterHelper.h"

@implementation GameCenterHelper

static GameCenterHelper *sharedHelper = nil;

+ (GameCenterHelper *) sharedInstance {
    if (!sharedHelper) {
        sharedHelper = [[GameCenterHelper alloc]init];
    }
    return sharedHelper;
}

- (id) init {
    if(self = [super init]) {
        NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
        [nc addObserver:self selector:@selector(authenticationChanged) name:GKPlayerAuthenticationDidChangeNotificationName object:nil];
    }
    return self;
}

- (void) authenticationChanged {
    if ([GKLocalPlayer localPlayer].isAuthenticated && !userAuthenticated) {
        NSLog(@"Authentication changed: player authenticated.");
        userAuthenticated = YES;
    }
    else if([GKLocalPlayer localPlayer].isAuthenticated && userAuthenticated) {
        NSLog(@"Authentication changed: player not authenticated.");
        userAuthenticated = NO;
    }
}

- (void) authenticateLocalUser: (UIViewController *)mainView {
    NSLog(@"Authenticating local player...");
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    localPlayer.authenticateHandler = ^(UIViewController *viewController, NSError *error){
        if ([GKLocalPlayer localPlayer].isAuthenticated) {
            NSLog(@"Authentication complete");
        }
        else if (viewController) {
            [mainView presentViewController:viewController animated:YES completion:nil];
        }
        else{
            NSLog(@"AUthentication failed");
        }
    };
}

- (void) findMatchWithMinPlayers:(int)minPlayers maxPlayers:(int)maxPlayers viewController:(UIViewController *)viewController delegate:(id<GameCenterHelperDelegate>)theDelegate {
    
    matchStarted = NO;
    _match = nil;
    _presentingViewController = viewController;
    _delegate = theDelegate;
    [_presentingViewController dismissViewControllerAnimated:NO completion:nil];
    
    GKMatchRequest *request = [[GKMatchRequest alloc]init];
    request.minPlayers = minPlayers;
    request.maxPlayers = maxPlayers;
    
    GKMatchmakerViewController *mmvc = [[GKMatchmakerViewController alloc]initWithMatchRequest:request];
    
    [_presentingViewController presentViewController:mmvc animated:YES completion:nil];
    
}

- (void) matchmakerViewControllerWasCancelled:(GKMatchmakerViewController *)viewController {
    [_presentingViewController dismissViewControllerAnimated:YES completion:nil];
}
    
- (void) matchmakerViewController:(GKMatchmakerViewController *)viewController didFailWithError:(NSError *)error {
    [_presentingViewController dismissViewControllerAnimated:YES completion:nil];
    NSLog(@"Error finding match: %@",error.localizedDescription);
}

- (void) matchmakerViewController:(GKMatchmakerViewController *)viewController didFindMatch:(GKMatch *)match {
    [_presentingViewController dismissViewControllerAnimated:YES completion:nil];
    _match = match;
    _match.delegate = self;
    if (!matchStarted && _match.expectedPlayerCount == 0) {
        NSLog(@"Ready to start the match!");
        matchStarted = YES;
    }
}

- (void) match:(GKMatch *)match didReceiveData:(NSData *)data fromPlayer:(NSString *)playerID {
    if (_match != match) {
        return;
    }
    [_delegate match:match didReceiveData:data fromPlayer:playerID];
}

-(void) match:(GKMatch *)match player:(NSString *)playerID didChangeState:(GKPlayerConnectionState)state {
    if (_match != match) {
        return;
    }
    
    switch (state) {
        case GKPlayerStateConnected:
            //handle new player connection
            NSLog(@"Player connected");
            if (!matchStarted && match.expectedPlayerCount == 0) {
                NSLog(@"Ready to start the match!");
            }
            break;
            
        case GKPlayerStateDisconnected:
            //a player just disconnected
            NSLog(@"Player disconnected");
            matchStarted = NO;
            [_delegate matchEnded];
            break;
            
        default:
            break;
    }
}

- (void) match:(GKMatch *)match didFailWithError:(NSError *)error {
    if (_match != match) {
        return;
    }
    NSLog(@"Match failed with error: %@",error.localizedDescription);
    matchStarted = NO;
    [_delegate matchEnded];
}

- (void) matchmakerViewController:(GKMatchmakerViewController *)viewController didReceiveAcceptFromHostedPlayer:(NSString *)playerID {
    
}

- (void) matchmakerViewController:(GKMatchmakerViewController *)viewController didFindPlayers:(NSArray *)playerIDs {
    
}

@end
