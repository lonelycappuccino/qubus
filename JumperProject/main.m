//
//  main.m
//  JumperProject
//
//  Created by Klemen Košir on 14/02/14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LCAppDelegate class]));
    }
}