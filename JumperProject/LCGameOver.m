//
//  LCGameOver.m
//  Qubus
//
//  Created by Klemen Košir on 17/02/14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import "LCGameOver.h"
#import "LCAdsHelper.h"
#import "LCMenuScene.h"

@implementation LCGameOver {
    CGFloat deviceFactor, colorHue;
    SKSpriteNode *bg;
    LCMenuScene *menuScene;
    SKLabelNode *highscoreLabel;
    UIImage *screenshotImage;
}

-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        deviceFactor = [self getDeviceFactor];
        
        bg = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithWhite:0.9 alpha:1.0] size:self.size];
        [bg setPosition:CGPointMake(self.size.width/2, self.size.height/2)];
        [self addChild:bg];
        
        SKSpriteNode *redStripe1 = [SKSpriteNode spriteNodeWithColor:[UIColor redColor] size:CGSizeMake(self.size.width, 70.0*deviceFactor)];
        [redStripe1 setPosition:CGPointMake(self.size.width/2, self.size.height/2 + (deviceFactor == 1.0 ? 50.0 : 103.0))];
        [self addChild:redStripe1];
        
        SKSpriteNode *scoreTitle = [SKSpriteNode spriteNodeWithImageNamed:@"scoreTitle"];
        [scoreTitle setName:@"scoreTitle"];
        [scoreTitle setScale:deviceFactor];
        [scoreTitle.texture setFilteringMode:SKTextureFilteringNearest];
        [redStripe1 addChild:scoreTitle];
        
        SKSpriteNode *redStripe2 = [SKSpriteNode spriteNodeWithColor:[UIColor redColor] size:CGSizeMake(self.size.width, 55.0*deviceFactor)];
        [redStripe2 setName:@"redStripe"];
        [redStripe2 setPosition:CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame)-80.0*deviceFactor)];
        [self addChild:redStripe2];
        
        SKSpriteNode *blueStripe = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithRed:0.0 green:144.0/255.0 blue:1.0 alpha:1.0] size:CGSizeMake(self.size.width, 55.0*deviceFactor)];
        [blueStripe setName:@"shareStripe"];
        [blueStripe setPosition:CGPointMake(self.size.width/2, self.size.height-(deviceFactor == 1.0 ? 95.0 : 167.0))];
        [self addChild:blueStripe];
        
        SKLabelNode *shareLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura-Medium"];
        [shareLabel setName:@"shareLabel"];
        shareLabel.fontColor = [UIColor whiteColor];
        shareLabel.text = @"SHARE";
        shareLabel.fontSize = 38.0*(deviceFactor == 1.0 ? 1.0 : 1.8);
        shareLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
        [blueStripe addChild:shareLabel];
        
        highscoreLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura-Medium"];
        [highscoreLabel setName:@"highscoreLabel"];
        highscoreLabel.fontColor = [UIColor whiteColor];
        highscoreLabel.text = @"";
        highscoreLabel.fontSize = 30.0*(deviceFactor == 1.0 ? 1.0 : 1.8);
        highscoreLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
        [redStripe2 addChild:highscoreLabel];
        
        SKLabelNode *scoreLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura-Medium"];
        [scoreLabel setName:@"scoreLabel"];
        scoreLabel.fontColor = [UIColor blackColor];
        scoreLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
        scoreLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
        scoreLabel.text = @"0";
        scoreLabel.fontSize = 50.0*(deviceFactor == 1.0 ? 1.0 : 1.8);
        scoreLabel.position = CGPointMake(self.size.width/2, self.size.height/2-20.0*deviceFactor);
        [self addChild:scoreLabel];
     
        SKSpriteNode *rateButton = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithRed:0.0 green:144.0/255.0 blue:1.0 alpha:1.0] size:CGSizeMake(110.0*deviceFactor, 45.0*deviceFactor)];
        [rateButton setPosition:CGPointMake(self.size.width/2, 60.0*deviceFactor)];
        [rateButton setName:@"rateButton"];
        [self addChild:rateButton];
        
        SKLabelNode *rateText = [SKLabelNode labelNodeWithFontNamed:@"Futura-Medium"];
        [rateText setFontColor:[UIColor whiteColor]];
        [rateText setFontSize:28.0*deviceFactor];
        [rateText setText:@"RATE"];
        [rateText setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
        [rateButton addChild:rateText];
        
        SKLabelNode *textLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura-Medium"];
        [textLabel setFontColor:[UIColor darkGrayColor]];
        [textLabel setFontSize:24.0*deviceFactor];
        [textLabel setText:@"Tap to restart"];
        [textLabel setPosition:CGPointMake(self.size.width/2, self.size.height/2-140.0*deviceFactor)];
        [self addChild:textLabel];
        [textLabel runAction:[SKAction repeatActionForever:[SKAction sequence:@[[SKAction scaleTo:0.96 duration:0.8],
                                                                                [SKAction scaleTo:1.0 duration:0.7]]]]];
    }
    return self;
}

-(void)setScreenshot:(SKTexture*)screenshot {
    SKSpriteNode *screenshotNode = [SKSpriteNode spriteNodeWithTexture:screenshot];
    [screenshotNode setPosition:CGPointMake(self.size.width/2, self.size.height/2)];
    [screenshotNode setSize:self.size];
    [screenshotNode setName:@"screenshot"];
    [self addChild:screenshotNode];
    
    SKLabelNode *infoLabel = [SKLabelNode labelNodeWithFontNamed:@"Futura-Medium"];
    [infoLabel setFontColor:[UIColor darkGrayColor]];
    [infoLabel setFontSize:15.0*deviceFactor];
    [infoLabel setText:@"Lonely Cappuccino"];
    [infoLabel setPosition:CGPointMake(self.size.width/2, 15*deviceFactor)];
    [self addChild:infoLabel];
    
    _screenshot = screenshot;
}

-(void)convertTextureToImage:(SKTexture*)texture {
    UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, YES, 0.0);
    SKView *view = [[SKView alloc] initWithFrame:self.view.frame];
    SKScene *scene = [SKScene sceneWithSize:self.view.bounds.size];
    
    SKSpriteNode *node = [SKSpriteNode spriteNodeWithTexture:texture];
    node.position = CGPointMake(scene.size.width/2, scene.size.height/2);
    node.size = scene.size;
    [scene addChild:node];
    
    SKSpriteNode *scoreBanner = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithHue:colorHue saturation:0.65 brightness:0.55 alpha:1.0] size:CGSizeMake(scene.size.width, 120.0*deviceFactor)];
    [scoreBanner setPosition:CGPointMake(scene.size.width/2, scene.size.height/4*3)];
    [scene addChild:scoreBanner];
    
    SKLabelNode *scoreText = [SKLabelNode labelNodeWithFontNamed:@"Futura-Medium"];
    [scoreText setFontColor:[UIColor whiteColor]];
    [scoreText setFontSize:85.0*deviceFactor];
    [scoreText setText:[NSString stringWithFormat:@"%d",_score]];
    [scoreText setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
    [scoreBanner addChild:scoreText];
    
    SKLabelNode *scoreText2 = [SKLabelNode labelNodeWithFontNamed:@"Futura-Medium"];
    [scoreText2 setFontColor:[UIColor whiteColor]];
    [scoreText2 setFontSize:20.0*deviceFactor];
    [scoreText2 setPosition:CGPointMake(0.0, -46.0*deviceFactor)];
    [scoreText2 setText:@"points"];
    [scoreText2 setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
    [scoreBanner addChild:scoreText2];
    
    SKSpriteNode *title = [SKSpriteNode spriteNodeWithImageNamed:@"title"];
    [title.texture setFilteringMode:SKTextureFilteringNearest];
    [title setScale:0.6*deviceFactor];
    [title setPosition:CGPointMake(self.size.width/2, self.size.height-title.frame.size.height/2-10.0)];
    [scene addChild:title];
    
    [view presentScene:scene];
    [view drawViewHierarchyInRect:view.frame afterScreenUpdates:YES];
    screenshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
}

-(CGFloat)getDeviceFactor {
    return ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone ? 1.0 : 2.0);
}

-(void)setColor:(UIColor *)color {
    [bg setColor:color];
    [color getHue:&colorHue saturation:nil brightness:nil alpha:nil];
}

-(void)didMoveToView:(SKView *)view {
    [[self childNodeWithName:@"screenshot"]runAction:[SKAction fadeAlphaTo:0.0 duration:0.35] completion:^{
        [[LCAdsHelper sharedInstance]showBannerWithPosition:0];
    }];
    [self removeAllGestures];
    [self performSelector:@selector(enableUserInteraction) withObject:nil afterDelay:0.5];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapHandler:)];
    [self.view addGestureRecognizer:tap];
    
    int highscore = (int)[[_data objectForKey:@"highscore"]integerValue];
    [(SKLabelNode*)[self childNodeWithName:@"scoreLabel"]setText:[NSString stringWithFormat:@"%d",_score]];
    [highscoreLabel setText:[NSString stringWithFormat:@"Best: %d",highscore]];
    if (_newHighscore) {
        [highscoreLabel runAction:[SKAction repeatActionForever:[SKAction sequence:@[[SKAction scaleTo:1.17 duration:0.3],
                                                                                                                           [SKAction scaleTo:1.0 duration:0.5]]]]];
    }
    menuScene = [LCMenuScene sceneWithSize:self.size];
    
    [self convertTextureToImage:_screenshot];
}

-(void)enableUserInteraction{
    [self.view setUserInteractionEnabled:YES];
}


-(void)removeAllGestures {
    for (UIGestureRecognizer *gesture in [self.view gestureRecognizers]) {
        [self.view removeGestureRecognizer:gesture];
    }
}

-(void)showGameCenter {
    GKGameCenterViewController *gameCenterViewController = [[GKGameCenterViewController alloc]init];
    if (gameCenterViewController != nil) {
        gameCenterViewController.gameCenterDelegate = self;
        [gameCenterViewController setViewState:GKGameCenterViewControllerStateDefault];
        UIViewController *viewController = self.view.window.rootViewController;
        [viewController presentViewController:gameCenterViewController animated:YES completion:nil];
    }
}

-(void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController {
    [gameCenterViewController dismissViewControllerAnimated:YES completion:nil];
}

-(void)tapHandler:(UITapGestureRecognizer*)gesture {
    CGPoint location = [self convertPointFromView:[gesture locationInView:self.view]];
    if ([[self childNodeWithName:@"shareStripe"]containsPoint:location]) {
        NSString *text = [NSString stringWithFormat:@"Just scored %d in Qubus. Can you beat me?!",_score];
        NSURL *url = [NSURL URLWithString:@"http://appstore.com/qubus"];
        
        UIActivityViewController *shareSheet =
        [[UIActivityViewController alloc]
         initWithActivityItems:@[text, url, screenshotImage]
         applicationActivities:nil];
        shareSheet.excludedActivityTypes = @[UIActivityTypePostToWeibo,
                                             UIActivityTypeMail,
                                             UIActivityTypePrint,
                                             UIActivityTypeCopyToPasteboard,
                                             UIActivityTypeAssignToContact,
                                             UIActivityTypeAddToReadingList,
                                             UIActivityTypePostToFlickr,
                                             UIActivityTypePostToVimeo,
                                             UIActivityTypePostToTencentWeibo,
                                             UIActivityTypeAirDrop,
                                            ];
        [self.view.window.rootViewController presentViewController:shareSheet animated:YES completion:nil];
    }
    else if ((CGRectContainsPoint(CGRectMake(0.0, 85.0*deviceFactor, self.size.width, self.size.height-((deviceFactor == 1.0 ? 95.0 : 167.0) + 100.0)*deviceFactor), location))) {
        [[LCAdsHelper sharedInstance]hideBanner];
        [self.view setUserInteractionEnabled:NO];
        [self.view presentScene:menuScene transition:[SKTransition revealWithDirection:SKTransitionDirectionUp duration:0.75]];
    }
    else if ([[self childNodeWithName:@"rateButton"]containsPoint:location]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id826047918"]];
    }
    
}

@end
