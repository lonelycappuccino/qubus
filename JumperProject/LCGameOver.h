//
//  LCGameOver.h
//  Qubus
//
//  Created by Klemen Košir on 17/02/14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <GAmeKit/GameKit.h>
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>

@interface LCGameOver : SKScene <GKGameCenterControllerDelegate,UIApplicationDelegate>

@property (nonatomic,assign) int score;
@property (nonatomic,strong) NSMutableDictionary *data;
@property (nonatomic,assign) BOOL newHighscore;
@property (nonatomic, strong) SKTexture *screenshot;

-(void)setColor:(UIColor*)color;

@end
