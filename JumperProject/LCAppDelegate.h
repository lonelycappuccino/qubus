//
//  LCAppDelegate.h
//  Qubus
//
//  Created by Klemen Košir on 14/02/14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface LCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
