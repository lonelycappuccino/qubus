//
//  LCMenuScene.h
//  Qubus
//
//  Created by Klemen Košir on 17/02/14.
//  Copyright (c) 2014 Lonely Cappuccino. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <Social/Social.h>
#import <GameKit/GameKit.h>
#import <AVFoundation/AVFoundation.h>
#import <OpenGLES/ES1/glext.h>
#import <QuartzCore/QuartzCore.h>

@interface LCMenuScene : SKScene <GKGameCenterControllerDelegate,SKPhysicsContactDelegate,UIApplicationDelegate>

@property (nonatomic,strong) NSMutableDictionary *data;


@end
