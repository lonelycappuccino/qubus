//
//  LCAdsHelper.m
//
//  Created by Klemen Košir on 07/02/14.
//  Copyright (c) 2014 Lonely Cappucino. All rights reserved.
//

#import "LCAdsHelper.h"

@implementation LCAdsHelper {
    CGRect initialAdFrame,adPosition;
    UIView *uiView;
    GADBannerView *adMobBanner;
    GADInterstitial *adMobInterstitial;
    ADBannerView *iAdBanner;
    ADInterstitialAd *iAdInterstitial;
    BOOL didSetAdPosition;
    NSArray *devices;
}

static LCAdsHelper *adsHelper = nil;

+(LCAdsHelper*)sharedInstance {
    if (!adsHelper) {
        adsHelper = [[LCAdsHelper alloc]init];
    }
    return adsHelper;
}

-(id)init {
    if (self = [super init]) {
        _iAdAvailable = NO;
        _bannerAdLoaded = NO;
        _interstitialAdLoaded = NO;
        _didShowInterstitialAd = NO;
        didSetAdPosition = NO;
        devices = @[ @"0e6e88a659d0263e3ffec3bb9be91ded", @"c510eb235e5cbe6af60049e88fc3d807", @"116c0fc2243678adf83ba0eb77e9f101", GAD_SIMULATOR_ID ];
    }
    return self;
}
/**
 positionInt:0 - TOP,1 - BOTTOM,-1 - HIDDEN
 */
-(void)initBannerOnViewController {
    //NSLog(@"INIT BANNER AD");
    if (_iAdAvailable) {
        [self initiAdBanner];
    }
    else {
        [self initAdMobBanner];
    }
}

-(void)initiAdBanner {
    iAdBanner = [[ADBannerView alloc]initWithAdType:ADAdTypeBanner];
    iAdBanner.delegate = self;
    initialAdFrame = iAdBanner.frame;
    //NSLog(@"FRAME: %f x %f",initialAdFrame.size.width,initialAdFrame.size.height);
    iAdBanner.frame = CGRectOffset(iAdBanner.frame, _viewController.view.bounds.size.width/2-iAdBanner.bounds.size.width/2, -iAdBanner.bounds.size.height);
    
    [_viewController.view addSubview:iAdBanner];
}

-(void)hideBanner {
    //NSLog(@"HIDE - INIT FRAME: %f x %f",initialAdFrame.size.width,initialAdFrame.size.height);
    if (iAdBanner) {
         iAdBanner.frame = CGRectOffset(initialAdFrame, _viewController.view.bounds.size.width/2-iAdBanner.bounds.size.width/2, -iAdBanner.bounds.size.height);
    }
    else {
        adMobBanner.frame = CGRectMake(_viewController.view.frame.size.height/2-adMobBanner.frame.size.width/2,
                                       -adMobBanner.frame.size.height, adMobBanner.frame.size.width, adMobBanner.frame.size.height);
    }
    didSetAdPosition = NO;
}
/**
 positionInt:0-TOP,1-BOTTOM
 */
-(void)showBannerWithPosition:(int)positionInt {
    //NSLog(@"SHOW - INIT FRAME: %f x %f",initialAdFrame.size.width,initialAdFrame.size.height);
    //NSLog(@"iAd Available: %d",_iAdAvailable);
    if (_iAdAvailable) {
        NSLog(@"show iad banner");
        switch (positionInt) {
            case 0:
                iAdBanner.frame = CGRectOffset(initialAdFrame, _viewController.view.bounds.size.width/2-iAdBanner.bounds.size.width/2, -iAdBanner.frame.size.height);
                adPosition = CGRectOffset(initialAdFrame, _viewController.view.bounds.size.width/2-iAdBanner.bounds.size.width/2, 0.0);
                break;
            case 1:
                iAdBanner.frame = CGRectOffset(initialAdFrame, _viewController.view.bounds.size.width/2-iAdBanner.bounds.size.width/2, _viewController.view.bounds.size.height);
                adPosition = CGRectOffset(initialAdFrame, _viewController.view.bounds.size.width/2-iAdBanner.bounds.size.width/2, _viewController.view.bounds.size.height-iAdBanner.bounds.size.height);
                break;
            default:
                break;
        }
        if (_bannerAdLoaded) {
            [UIView beginAnimations:@"bannerSlide" context:nil];
            iAdBanner.frame = adPosition;
            [UIView commitAnimations];
        }
    }
    else {
        NSLog(@"show admob banner");
        switch (positionInt) {
            case 0:
                adMobBanner.frame = CGRectMake(_viewController.view.frame.size.
                                               width/2-adMobBanner.frame.size.width/2,
                                               -adMobBanner.frame.size.height, adMobBanner.frame.size.width, adMobBanner.frame.size.height);
                adPosition = CGRectMake(_viewController.view.frame.size.width/2-adMobBanner.frame.size.width/2,
                                        0.0, adMobBanner.frame.size.width, adMobBanner.frame.size.height);
                break;
            case 1:
                adMobBanner.frame = CGRectMake(_viewController.view.frame.size.width/2-adMobBanner.frame.size.width/2,
                                             _viewController.view.frame.size.height, adMobBanner.frame.size.width, adMobBanner.frame.size.height);
                adPosition = CGRectMake(_viewController.view.frame.size.width/2-adMobBanner.frame.size.width/2,
                                        _viewController.view.frame.size.height-adMobBanner.frame.size.height, adMobBanner.frame.size.width, adMobBanner.frame.size.height);
                break;
            default:
                break;
        }
        if (_bannerAdLoaded) {
            NSLog(@"admob loaded");
            [UIView beginAnimations:@"bannerSlide" context:nil];
            adMobBanner.frame = adPosition;
            [UIView commitAnimations];
        }
    }
    didSetAdPosition = YES;
}

-(void)initInterstitialAdOnViewController {
    if (_didShowInterstitialAd) {
        return;
    }
    //NSLog(@"INIT INTERSTITAL AD");
    if (!iAdInterstitial && [[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        iAdInterstitial = [[ADInterstitialAd alloc]init];
        iAdInterstitial.delegate = self;
        uiView = [[UIView alloc]initWithFrame:_viewController.view.bounds];
         [_viewController.view addSubview:uiView];
    }
    else {
        [self initAdMobInterstitial];
    }
}

-(void)presentInterstitialAd {
    if (_didShowInterstitialAd) {
        return;
    }
    if (_iAdAvailable && [[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        //[_viewController.view addSubview:uiView];
        [iAdInterstitial presentInView:uiView];
        //NSLog(@"Did present interstitial iAd");
    }
    else {
        [adMobInterstitial presentFromRootViewController:_viewController];
        //NSLog(@"Did present interstitial AdMob");
    }
    _didShowInterstitialAd = YES;
}

-(void)interstitialAdDidLoad:(ADInterstitialAd *)interstitialAd {
    _interstitialAdLoaded = YES;
    //NSLog(@"DID LOAD INTERSTITIAL iAD");
}

-(void)interstitialAdDidUnload:(ADInterstitialAd *)interstitialAd {
    //NSLog(@"DID UNLOAD INTERSTITIAL iAD");
}

-(void)interstitialAdActionDidFinish:(ADInterstitialAd *)interstitialAd {
    //NSLog(@"DID FINISH INTERSTITIAL iAD");
    [uiView removeFromSuperview];
    [self initBannerOnViewController];
}

-(void)interstitialAd:(ADInterstitialAd *)interstitialAd didFailWithError:(NSError *)error {
    //NSLog(@"DID FAIL INTERSTITIAL iAD");
    iAdInterstitial = nil;
    _iAdAvailable = NO;
    [self initAdMobInterstitial];
}

-(void)initAdMobBanner {
    if (adMobBanner) {
        return;
    }
    _iAdAvailable = NO;
    adMobBanner = [[GADBannerView alloc]initWithAdSize:(([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPhone) ? kGADAdSizeBanner : kGADAdSizeLeaderboard)];
    adMobBanner.adUnitID = @"ca-app-pub-7214617058506625/2105206995";
    
    adMobBanner.delegate = self;
    adMobBanner.frame = CGRectMake(_viewController.view.frame.size.width/2-adMobBanner.frame.size.width/2,
                                   -adMobBanner.frame.size.height, adMobBanner.frame.size.width, adMobBanner.frame.size.height);
    adMobBanner.rootViewController = _viewController;
    [_viewController.view addSubview:adMobBanner];
    GADRequest *request = [GADRequest request];
    //--- ENABLE TESTING ---//
    request.testDevices = devices;
    
    [adMobBanner loadRequest:request];
}

-(void)initAdMobInterstitial {
    if (adMobInterstitial) {
        return;
    }
    //NSLog(@"INIT ADMOB INTERSTITIAL AD");
    adMobInterstitial = [[GADInterstitial alloc]init];
    adMobInterstitial.delegate = self;
    adMobInterstitial.adUnitID = @"ca-app-pub-7214617058506625/8119021392";
    GADRequest *request = [GADRequest request];
    //--- ENABLE TESTING ---//
    request.testDevices = devices;
    
    [adMobInterstitial loadRequest:request];
    
}

-(void)interstitialDidReceiveAd:(GADInterstitial *)ad {
    _interstitialAdLoaded = YES;
    //NSLog(@"DID RECEIVE INTERSTITIAL ADMOB");
}

-(void)interstitialDidDismissScreen:(GADInterstitial *)ad {
    [self initBannerOnViewController];
}

//error=3 : no ad inventory | error=2 : network problem
-(void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error {
    //NSLog(@"AdMob Banner error: %@",error);
    [view removeFromSuperview];
    [self initBannerOnViewController];
}

-(void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error {
    //NSLog(@"AdMob Interstitial error: %@",error);
    if (!adMobBanner) {
        [self initAdMobBanner];
    }
}

-(void)adMobBannerRequest {
    if (_bannerAdLoaded) {
        return;
    }
    GADRequest *request = [GADRequest request];
    //--- ENABLE TESTING ---//
    request.testDevices = devices;
    [adMobBanner loadRequest:request];
}

-(void)adMobInterstitialRequest {
    if (_bannerAdLoaded) {
        return;
    }
    GADRequest *request = [GADRequest request];
    //--- ENABLE TESTING ---//
    request.testDevices = devices;
    [adMobInterstitial loadRequest:request];
}

-(void)adViewDidReceiveAd:(GADBannerView *)view {
    _bannerAdLoaded = YES;
    //NSLog(@"DID RECEIVE ADMOB BANNER AD");
    if (didSetAdPosition) {
        [UIView beginAnimations:@"bannerSlide" context:nil];
        view.frame = adPosition;
        [UIView commitAnimations];
    }
}

-(void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error {
    //NSLog(@"iAd Banner error: %@",error);
    [banner removeFromSuperview];
    iAdBanner = nil;
    _iAdAvailable = NO;
    [self initAdMobBanner];
}

-(void)bannerViewDidLoadAd:(ADBannerView *)banner {
    //NSLog(@"iAD BANNER DID LOAD");
    _bannerAdLoaded = YES;
    //[self showBannerWithPosition:_adPositionInt];
    if (didSetAdPosition) {
        [UIView beginAnimations:@"bannerSlide" context:nil];
        banner.frame = adPosition;
        [UIView commitAnimations];
    }
}

@end
